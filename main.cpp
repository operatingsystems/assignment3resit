/**
  * Assignment: memory
  * Operating Systems
  */

// function/class definitions you are going to use
#include <sys/resource.h>
#include <sys/time.h>

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <vector>

// although it is good habit, you don't have to type 'std::' before many objects by including this line
using namespace std;

// ADJUST BELOW
const int64_t L = 16384ULL;
const int64_t M = 52ULL;
const int64_t N = 16384ULL;
const int64_t REPEAT = 1ULL;
// ADJUST ABOVE

int main(int argc, char* argv[]) {
	// matrices: A * B -> C
	uint32_t* a = new uint32_t[L * M];
	uint32_t* b = new uint32_t[M * N];
	uint32_t* c = new uint32_t[L * N];

	// fill with dummy data
	for (int64_t i = 0; i < L*M; i++) {
		a[i] = ((16384-i) << 16) | (i & 0xFFFF);
	}
	for (int64_t i = 0; i < M*N; i++) {
		b[i] = ((12832-i) << 16) | (i & 0xFFFF);
	}
	// this dummy value is needed to avoid compilers eliminating the loop as part of a optimisation
	uint64_t dummy = 0;
	for (int64_t z = 0; z < REPEAT; z++) {
    // ADJUST BELOW, BUT keep writing to the dummy variable

		// matrices: A * B -> C
		for (int64_t i = 0; i < N; i++) {
			for (int64_t j = 0; j < L; j++) {
				c[i*L + j] = 0;
				for (int64_t k = 0; k < M; k++) {
					auto av = a[k*L + j];
					auto bv = b[i*M + k];
					c[i*L + j] += bv * av;
				}
			}
		}

		// DO NOT MERGE THIS LOOP WITH LOOP ABOVE (although this loop can be optimized)
		// dummy needs to be written to (to check if result changed, and to avoid optimizations)
		for (int64_t i = 0; i < N; i++) {
			for (int64_t j = 0; j < L; j++) {
				dummy += c[j*N + i];
			}
		}
    // ADJUST ABOVE, BUT keep writing to the dummy variable
	}

	delete[] a;
	delete[] b;
	delete[] c;

	struct rusage usage;
	getrusage(RUSAGE_SELF, &usage);

	std::cout << "user time:                    " << usage.ru_utime.tv_sec << "." << std::fixed << std::setw(6) << std::setprecision(6) << std::setfill('0') << usage.ru_utime.tv_usec << " s" << std::endl;
	std::cout << "soft page faults:             " << usage.ru_minflt << std::endl;
	std::cout << "hard page faults:             " << usage.ru_majflt << std::endl;
#ifdef __APPLE__
	std::cout << "max memory:                   " << usage.ru_maxrss/1024 << " KiB" << std::endl;
#else
	std::cout << "max memory:                   " << usage.ru_maxrss << " KiB" << std::endl;
#endif
	std::cout << "voluntary context switches:   " << usage.ru_nvcsw << std::endl;
	std::cout << "involuntary context switches: " << usage.ru_nivcsw << std::endl;
	std::cout << "dummy value (ignore):         " << dummy << std::endl; // this value is printed to avoid optimisations
  std::cout << "typical page size:            " << getpagesize() << std::endl;
	// ...
	return 0;
}
